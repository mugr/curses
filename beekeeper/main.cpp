#include <unistd.h>

#include <curses.h>

int
main(){
  initscr();
  WINDOW *window = newwin(0, 0, 0, 0);
  for(;;){
    werase(window);
    box(window, ACS_VLINE, ACS_HLINE);
    wrefresh(window);
    sleep(1);
  }
}
